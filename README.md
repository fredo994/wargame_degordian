# Battle simulation
## Description:
Simple simulation of two armies fighting. An outcome of the battle is unknown and can be one of 6 things.

* Both armies surrendered.
* Red army surrendered
* Blue army surrendered
* Both armies are destroyed ( most unlikely  -> gold nugget if you see this )
* Red army is destroyed
* Blue army is destroyed

Battle is fought in iterations. First, one of two armies is chosen with 50 % chance to be first army to attack.
Than armies attack each other one after the other iteratively ( ping pong action ).
Battle is fought until one of stopping criteria is met( described above ).

Army consists of smaller army divisions which consists of soldiers. 
I have implemented 7 types of soldiers. Each soldier has its owns ability to attack( or not to attack like hippie ).
Ability of each soldier is described in its respective class.

A battle has its own story. (I have provided story about alien invasion). Story format is described in Util/StoryReader.

## Usage: 
To run the simulation with default story just run "BattleMain" class.
 
 A battle can be logged. A log file will be written in resources folder ( set in BattleMain )
 
If you want to create a new story and run it with default main, just add a story, written in described story format, and place it in resources folder. Than change path of the story in the main and run it. 