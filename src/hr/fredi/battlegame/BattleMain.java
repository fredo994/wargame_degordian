package hr.fredi.battlegame;

import hr.fredi.battlegame.Util.BattleLogger;
import hr.fredi.battlegame.Util.StoryReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import static hr.fredi.battlegame.Army.*;

/**
 *
 * Created by Fredi Šarić on 11.12.15.
 */
public class BattleMain {

    /**
     * Path to the story
     */
    private static final String STORY = "resources/EvilAlienStory";

    /**
     * Log path
     */
    private static final String LOG_PATH = "resources/log.txt";

    /**
     * Shows a story and runs a battle after user enters
     * battle parameter.
     *
     * @param args null
     * @throws IOException if problem occur during story reading
     */
    public static void main(String[] args) throws IOException {


        StoryReader sr = new StoryReader();
        sr.read(Files.readAllLines(Paths.get(STORY)));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        sr.getIntro().forEach(System.out::println);
        int armySize  = enterArmySize(reader);
        int numOfDivs = enterDivisionNumber(reader);

        sr.getStoryEnd().forEach(System.out::println);

        boolean log = getLog(reader);

        Battle battle = new Battle(armySize, numOfDivs, log);
        int result = battle.battle();

        sr.getResponse(result).forEach(System.out::println);
        if(log) {
            BattleLogger.getInstance().saveLog(LOG_PATH);
        }
    }

    /**
     * Ask a user for number of divisions in an army.
     * @param reader user input reader.
     * @return number of army divisions.
     */
    private static int enterDivisionNumber(BufferedReader reader) {
        do {
            try {
                System.out.println("Enter number of army divisions: ");
                int armyDivisons = Integer.parseInt(reader.readLine().trim());
                if(armyDivisons < MIN_DIVISION_SIZE) {
                    System.out.println("Number of divisions too small.");
                    System.out.println("Minimal number of divisons  is " + MIN_ARMY_SIZE);
                    continue;
                } else if (armyDivisons > MAX_DIVISION_SIZE) {
                    System.out.println("Number of divisions too big.");
                    System.out.println("Maximal number of divisons is " + MIN_ARMY_SIZE);
                    continue;
                }

                return armyDivisons;
            }catch (Exception ex){
                System.out.println("Invalid size");
            }
        } while (true);

    }

    /**
     * Asks a user for army size.
     * @param reader user input reader.
     * @return army size
     */
    private static int enterArmySize(BufferedReader reader) {
        do {
            try {
                System.out.println("Enter army size");
                int armySize = Integer.parseInt(reader.readLine().trim());

                if(armySize < MIN_ARMY_SIZE) {
                    System.out.println("Army size too small.");
                    System.out.println("Minimal army size is " + MIN_ARMY_SIZE);
                    continue;
                } else if (armySize > MAX_ARMY_SIZE) {
                    System.out.println("Army size too big.");
                    System.out.println("Maximal army size is " + MAX_ARMY_SIZE);
                    continue;
                }

                return armySize;
            }catch (Exception ex){
                System.out.println("Invalid size");
            }
        } while (true);
    }

    /**
     * Ask a user if he wishes to log the battle or not
     *
     * @param reader user input reader
     * @return true if user wants to log the battle, false otherwise
     */
    private static boolean getLog(BufferedReader reader) {
        do {
            try {
                System.out.println( "Do you want to log battle:(yes/no) ");
                String readLine = reader.readLine();
                if(readLine.trim().equalsIgnoreCase("yes")) {
                    return true;
                } else if(readLine.trim().equalsIgnoreCase("no")){
                    return false;
                } else {
                    System.out.println("Invalid input pleas enter again.");
                }
            } catch (Exception ex) {
                System.out.println("Problem while reading please enter again.");
            }
        } while (true);
    }

}
