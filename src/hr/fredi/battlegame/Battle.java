package hr.fredi.battlegame;
import hr.fredi.battlegame.Util.BattleLogger;
import hr.fredi.battlegame.soldiers.Soldier;

import java.util.List;
import java.util.Random;

/**
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class Battle {

    /**
     * Indicates that battle has not finished jet.
     */
    private static final int NOT_FINISHED = 0;

    /**
     * Indicates that battle has finished with both
     * armies surrendering.
     */
    public static final int PEACE = 1;

    /**
     * Indicates that red army has surrendered
     */
    public static final int RED_SURRENDERED = 2;

    /**
     * Indicates that blue army has surrendered
     */
    public static final int BLUE_SURRENDERED = 3;

    /**
     * Indicates that both armies has been destroyed.
     */
    public static final int BOTH_DESTROYED = 4;

    /**
     * Indicates that only red army has been destroyed
     */
    public static final int RED_DESTROYED = 5;

    /**
     * Indicates that only red army has been destroyed
     */
    public static final int BLUE_DESTROYED = 6;

    /**
     * Red army
     */
    private Army redArmy;

    /**
     * Blue army
     */
    private Army blueArmy;

    /**
     * Random number generator
     */
    private Random rnd;

    /**
     * Flag used to determine will battle be logged or not.
     */
    private boolean log;

    /**
     * Construct a battle. In Battle participate two armies, red
     * and blue army. Armies will be of given size. If log flag is
     * true than battle will be logged.
     *
     * @param armySize army size.
     * @param divisionNumbers number of divisions
     * @param log if true battle will be logged, if false it will not be
     */
    public Battle(int armySize, int divisionNumbers, boolean log) {
        rnd = new Random();
        this.log = log;

        redArmy  = new Army(armySize, divisionNumbers);
        blueArmy = new Army(armySize, divisionNumbers);
    }

    /**
     * Starts a battle.
     * Both armies have equal chance of being chosen to be first to
     * attack.
     *
     * @return outcome of the battle.
     */
    public int battle() {
        int over = NOT_FINISHED;
        boolean start = rnd.nextBoolean();
        Army first  = start ? redArmy  : blueArmy;
        Army second = start ? blueArmy : redArmy;
        BattleLogger log = BattleLogger.getInstance();
        while(true) {
            log.prepareLog(start ? "RED: " : "BLUE: ");
            attack(first, second);  //Red army attacks
            if((over = isOver()) != NOT_FINISHED) break;

            log.prepareLog(start ? "BLUE " : "RED: ");
            attack(second, first);  //Blue army attacks
            if((over = isOver()) != NOT_FINISHED) break;
        }
        return over;
    }

    /**
     * Chooses a soldier from attackerArmy and a soldier from defender
     * army, and make them fight.
     * Logs fight outcome if necessary.
     *
     * @param attackerArmy army that is attacking other army
     * @param defenseArmy army that is defending it self
     */
    private void attack(Army attackerArmy, Army defenseArmy) {
        Soldier attacker = getSoldier(attackerArmy);
        Soldier defender = getSoldier(defenseArmy);

        String result = attacker.attack(defender);

        if(log) {
            BattleLogger.getInstance().addLog(result);
        }
    }

    /**
     * Takes a random soldier from an army.
     * @param army army from whom soldier will be picked.
     * @return a random soldier from given army.
     */
    private Soldier getSoldier(Army army) {
        List<ArmyDivision> divisions = army.getDivisions();
        List<Soldier> soldiers = divisions.get(rnd.nextInt(divisions.size())).getSoldiers();

        return soldiers.get(rnd.nextInt(soldiers.size()));  //Choose random soldier from division
    }

    /**
     * Multiple scenarios can occur that indicate that battle is over.
     * Battle over conditions:
     * <ul>
     *     <li>Both armies surrendered: peace was made.</li>
     *     <li>One army surrendered: general made decision</li>
     *     <li>Both armies are destroyed. (Kamikaze destroyed last two divisions)</li>
     *     <li>One army is completely destroyed</li>
     * </ul>
     *
     *
     * @return true is battle is over.
     */
    private int isOver() {
        //If one or both armies has serenaded
        if(redArmy.hasSurrendered() && blueArmy.hasSurrendered()) {
            return PEACE;
        } else if(redArmy.hasSurrendered()) {
            return RED_SURRENDERED;
        } else if(blueArmy.hasSurrendered()){
            return BLUE_SURRENDERED;
        }

        //If one or both armies has been destroyed
        if(redArmy.isDestroyed() && blueArmy.isDestroyed()) {
            return BOTH_DESTROYED;
        } else if(redArmy.isDestroyed()) {
            return RED_DESTROYED;
        } else if(blueArmy.isDestroyed()) {
            return BLUE_DESTROYED;
        }

        return NOT_FINISHED;
    }

}
