package hr.fredi.battlegame.soldiers;

import java.util.Random;

/**
 * Kamikaze soldier type destroys whole division of the other soldier,
 * but he can also destroys his division with a chance of 50%, because
 * he was not trained enough.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class Kamikaze extends Soldier {

    /**
     * Random number generator
     */
    private Random rnd;

    /**
     * Constructs a kamikaze
     * @param damage kamikaze damage - not being used
     * @param health kamikaze health
     * @param name name of kamikaze
     * @param rnd random number generator
     */
    public Kamikaze(int damage, int health, String name, Random rnd) {
        super(damage, health, name);
        this.rnd = rnd;
    }

    @Override
    public String attack(Soldier soldier) {
        if(rnd.nextBoolean()) { //Kamikaze chose to detonate bomb
            reduceHealth(getHealth()); //Killed him self
            soldier.getDivision().destroyDivision();

            if(rnd.nextBoolean()) {

                getDivision().destroyDivision();
                return getName() +" destroyed enemy division, " +
                       "but his division was also destroyed, because he did not properly made his bomb.";
            }

            return getName() + " destroyed enemy division.";
        }

        return getName() + " got scared and did not activate bomb.";
    }
}
