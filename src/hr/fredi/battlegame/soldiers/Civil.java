package hr.fredi.battlegame.soldiers;

/**
 * Civilian is a soldier that will only attack other civilians
 * and old mans.
 * If he is apposed ba military he will not attack.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class Civil extends Soldier{

    /**
     * Constructs a civilian.
     * @param damage civilian damage
     * @param health civilian health
     * @param name name of the civilian
     */
    public Civil(int damage, int health, String name) {
        super(damage, health, name);
    }

    @Override
    public String attack(Soldier soldier) {

        if(soldier instanceof Civil || soldier instanceof OldMan) {
            soldier.reduceHealth(getDamage());
            if(soldier.getHealth() == 0) {
                return getName() + " killed " + soldier.getName();
            }
            return getName() + " reduced health of " + soldier.getName() + " to " + soldier.getHealth();
        } else { //Civilian will not attack military
            return getName() + " did not attack " + soldier.getName();
        }
    }
}
