package hr.fredi.battlegame.soldiers;

import java.util.Random;

/**
 * Sniper becomes more successful with every shoot he
 * hits.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class Sniper extends Soldier{

    /**
     * Chance that sniper will shoot his target
     */
    private double hitPercent;

    /**
     * Rate at which sniper will increase his skills
     */
    private double learnRate;

    /**
     * Random number generator
     */
    private Random rnd;

    /**
     * Creates sniper
     * @param damage sniper damage, usually high
     * @param health sniper health
     * @param name name of the sniper
     * @param hitPercent base chance of hitting an opponent
     * @param rnd random number generator
     */
    public Sniper(int damage, int health, String name, double hitPercent, Random rnd) {
        super(damage, health, name);
        this.hitPercent = hitPercent;
        learnRate = (hitPercent - 1) / (hitPercent - 2); // Calculated from geometric series
        this.rnd = rnd;
    }

    @Override
    public String attack(Soldier soldier) {
        double rndVal = rnd.nextDouble();

        if(rndVal < hitPercent) {
            //Increase accuracy
            hitPercent += learnRate;
            learnRate  += learnRate;
            soldier.reduceHealth(getDamage());

            return getName() + " shot " + soldier.getName() + " and increased its accuracy to " + hitPercent*100 + "%.";
        }

        return getName() + " missed.";
    }
}
