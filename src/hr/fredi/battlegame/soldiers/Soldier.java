package hr.fredi.battlegame.soldiers;

import hr.fredi.battlegame.ArmyDivision;

/**
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public abstract class Soldier {

    /**
     * Damage that this soldier makes
     */
    private int damage;

    /**
     * Amount of health this soldier has
     */
    private int health;

    /**
     * Name of the soldier.
     */
    private String name;

    /**
     * Division that this soldier belongs to.
     */
    private ArmyDivision division;

    /**
     * Creates a soldier.
     * @param damage damage that it make when attacking
     * @param health ammount of health that it has at the begging
     * @param name name of the soldier
     */
    public Soldier(int damage, int health, String name) {
        this.damage = damage;
        this.health = health;
        this.name = name;
    }

    /**
     * Returns a soldier damage.
     * @return a soldier damage.
     */
    public int getDamage() {
        return damage;
    }

    /**
     * Returns a soldier health
     * @return a soldier health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Returns a soldier name.
     * @return a soldier name.

     */
    public String getName() { return name; }

    /**
     * Returns a soldier division.
     * @return a soldier division.
     */
    public ArmyDivision getDivision() {
        return division;
    }

    /**
     * Reduces health of the soldier by given amount, or makes
     * it 0 if the soldier is killed.
     *
     * @param amount amount of damage soldier will take
     */
    public void reduceHealth(int amount) {
        health -= amount;
        if(health <= 0) { //Killed
            health = 0;   //Health can not be negative
            division.getSoldiers().remove(this);
        }
    }

    /**
     * Attacks given soldier.
     * Type of soldier attack is implementation dependant.
     *
     * @param soldier soldier being attacked.
     * @return result of the battle in form of a String
     */
    public abstract String attack(Soldier soldier);

    /**
     * Places this soldier to given division.
     * @param division division of the soldier
     */
    public void setDivision(ArmyDivision division) {
        this.division = division;
    }
}
