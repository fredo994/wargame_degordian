package hr.fredi.battlegame.soldiers;

import java.util.Random;

/**
 * Old man has 50% chance that before he goes to
 * fight broke his hip and dies. But if he survives
 * to the fight he has 50% chance of being injured
 * and 50% of dealing damage.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class OldMan extends Soldier{

    /**
     * Random number generator
     */
    private Random rnd;

    /**
     * Constructs an old man.
     * @param damage old man damage
     * @param health old man health - usualy small
     * @param name old man health
     * @param rnd random number generator
     */
    public OldMan(int damage, int health, String name, Random rnd) {
        super(damage, health, name);
        this.rnd = rnd;
    }

    @Override
    public String attack(Soldier soldier) {

        if(rnd.nextBoolean()) { //50% of being injured and dieing
            reduceHealth(getHealth());
            return getName() + " fell and broke his hip. He died.";
        } else {
            if(rnd.nextBoolean()) { //50% chance of making demage
                soldier.reduceHealth(getDamage());

                if(soldier.getHealth() == 0) {
                  return getName() + " killed " + soldier.getName();
                }

                return getName() + " reduced health of " + soldier.getName() + " to " + soldier.getHealth();
            } else {                //50% chance of being damaged
                reduceHealth(soldier.getDamage());

                if(getHealth() == 0) {
                    return getName() + " was killed in battle with " + soldier.getName();
                }

                return getName() + " was damaged in battle by " + soldier.getName() + " and has " + getHealth() + " left";
            }
         }
    }
}
