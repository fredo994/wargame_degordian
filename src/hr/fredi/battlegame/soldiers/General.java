package hr.fredi.battlegame.soldiers;

import hr.fredi.battlegame.Army;

import java.util.Random;

/**
 * General has a chance to surrender if he sees that he is loosing.
 * If he attacks other general he has a chance of making peace with
 * other side and ending battle.
 * Or he attacks like normal soldier.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class General extends Soldier {

    /**
     * Chance that general will end battle if he attacks other
     * general
     */
    private double peaceChance;

    /**
     * If general sees that his army has been decreased under
     * <code>considerSurrender * originalArmySize</code> he
     * will consider surrender.
     */
    private double considerSurrender;

    /**
     * Chance that general will surrender if he considers his
     * surrender.
     */
    private double surrenderChance;

    /**
     * Random number generator.
     */
    private Random rnd;

    /**
     * Construct a general
     * @param damage damage which general makes
     * @param health health of the general
     * @param name name of the general
     * @param peaceChance chance of peace
     * @param considerSurrender consideration of surrender value
     * @param surrenderChance chance of surrender
     * @param rnd random number generator
     */
    public General(int damage, int health, String name, double peaceChance, double considerSurrender, double surrenderChance,Random rnd) {
        super(damage, health, name);
        this.peaceChance = peaceChance;
        this.considerSurrender = considerSurrender;
        this.surrenderChance = surrenderChance;
        this.rnd = rnd;
    }

    @Override
    public String attack(Soldier soldier) {
        Army a = getDivision().getArmy();
        if(considerSurrender > a.getSize() / (double) a.getStartSize()) { //Consider surrender
            if(surrenderChance > rnd.nextDouble()) { //Surrendered
                a.surrender();
                return getName() + " surrendered.";
            }
        }

        if(soldier instanceof General) {
            if(tryToEndBattle(soldier)) { //If peace was made
                return "Peace was made by conversation.";
            }
        }
        soldier.reduceHealth(getDamage());

        if(soldier.getHealth() == 0) {
            return soldier.getName() + " was killed by " + getName();
        }

        return soldier.getName() + " was injured by " + getName() + ", and its health is "  + soldier.getHealth();
    }

    /**
     * Tries to end battle with a other general
     * @param soldier other general
     * @return true if peace was made
     */
    private boolean tryToEndBattle(Soldier soldier) {

        double rndVal = rnd.nextDouble();

        if(peaceChance > rndVal) {
            getDivision().getArmy().surrender();
            soldier.getDivision().getArmy().surrender();
            return true;
        }

        return false;
    }
}
