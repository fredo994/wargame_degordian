package hr.fredi.battlegame.soldiers;

/**
 * Hippie is most useless soldier. He gets damaged when opposed by
 * other soldier that is not also hippie.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class Hippie extends Soldier {

    /**
     * Constructs hippie.
     * @param damage hippie damage
     * @param health hippie health
     * @param name name of the hippie
     */
    public Hippie(int damage, int health, String name) {
        super(damage, health, name);
    }

    @Override
    public String attack(Soldier soldier) {

        if(soldier instanceof Hippie) {
            return getName() + " found another hippie and did not make war.";
        }

        reduceHealth(soldier.getDamage());

        if(getHealth() == 0) {
            return getName() + " was killed by " + soldier.getName() + " because he did not want to engage in battle";
        }

        return getName() + " was injured by " + soldier.getName() + ", and his health is now " + getHealth();
    }
}
