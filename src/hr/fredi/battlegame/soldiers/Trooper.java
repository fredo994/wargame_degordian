package hr.fredi.battlegame.soldiers;


/**
 * Trooper is basic soldier.
 * When he attacks he deals damage to other soldier.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class Trooper extends Soldier {

    /**
     * Constructs a trooper.
     * @param damage trooper damage
     * @param health trooper health
     * @param name name of the trooper
     */
    public Trooper(int damage, int health, String name) {
        super(damage, health, name);
    }

    @Override
    public String attack(Soldier soldier) {
        soldier.reduceHealth(getDamage());

        if(soldier.getHealth() == 0) {
            return getName() + " killed " + soldier.getName();
        }

        return getName() + " reduced health of " + soldier.getName() + " to " + soldier.getHealth();
    }
}
