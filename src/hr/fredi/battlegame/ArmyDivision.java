package hr.fredi.battlegame;

import hr.fredi.battlegame.soldiers.Soldier;
import javafx.collections.ListChangeListener;
import javafx.collections.ModifiableObservableListBase;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class ArmyDivision {

    /**
     * Soldiers that belong to this army division.
     */
    ArrayObservableList<Soldier> soldiers;

    /**
     * Reference to an army
     */
    private Army army;

    /**
     * Destroy listeners
     */
    private List<DivisionDestroyListener> listeners;

    /**
     * Constructor
     * @param army division army
     */
    public ArmyDivision(Army army) {
        this.army = army;
        this.soldiers = new ArrayObservableList<>();
    }

    /**
     * Destroys this division
     */
    public void destroyDivision() {
        soldiers.clear();
    }

    /**
     * Adds a soldier to this division
     * @param soldier soldier that is going to be added to this division.
     */
    public void addSoldier(Soldier soldier) {
        soldiers.add(soldier);
        soldier.setDivision(this);
    }

    /**
     * Adds a collection of soldiers to this division.
     * @param soldiers soldiers that are going to be added to this division.
     */
    public void addSoldiers(Collection<Soldier> soldiers) {
        this.soldiers.addAll(soldiers);
        for (Soldier s : soldiers) {
            s.setDivision(this);
        }
    }

    /**
     * Registers given listener for division destroy events.
     * @param listener listener that is registering
     */
    public void addDivisionDestroyListener(DivisionDestroyListener listener) {
        if(listeners == null) {
            listeners = new ArrayList<>();
            soldiers.addListener((ListChangeListener<Soldier>) c -> {
                if (soldiers.isEmpty()) { //Division destroyed
                    for (DivisionDestroyListener l : listeners) {
                        listener.divisionDestroyed();
                    }
                }
            });
        }
        listeners.add(listener);
    }

    /**
     * Returns a list of soldiers in this division.
     * @return a list of soldiers in this division.
     */
    public List<Soldier> getSoldiers() {
        return soldiers;
    }

    /**
     * Returna an army that this division belongs to.
     * @return an army that this division belongs to.
     */
    public Army getArmy() {
        return army;
    }

    /**
     * Implementation of observable list(as suggested in ModifiableObservableListBase)
     * @param <E> elements type
     */
    private class ArrayObservableList<E> extends ModifiableObservableListBase<E> {

        private final List<E> delegate = new ArrayList<>();

        public E get(int index) {
            return delegate.get(index);
        }

        public int size() {
            return delegate.size();
        }

        protected void doAdd(int index, E element) {
            delegate.add(index, element);
        }

        protected E doSet(int index, E element) {
            return delegate.set(index, element);
        }

        protected E doRemove(int index) {
            return delegate.remove(index);
        }
    }

    /**
     * Listener that listens to division destruction.
     */
    public interface DivisionDestroyListener {

        /**
         * This method is called when division has no more alive
         * soldiers.
         */
        public void divisionDestroyed();
    }
}