package hr.fredi.battlegame;

import hr.fredi.battlegame.Util.SoldierFactory;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Created by Fredi Šarić on 11.12.15.
 */
public class Army {

    /**
     * Minimal allowed numbers of divisions
     */
    public static final int MIN_DIVISION_SIZE = 1;

    /**
     * Maximal allowed number of divisions
     */
    public static final int MAX_DIVISION_SIZE = 10_000;

    /**
     *  Minimal army size
     */
    public static final int MIN_ARMY_SIZE  = 10;

    /**
     * Maximal army size
     */
    public static final int MAX_ARMY_SIZE  = 100_000_000;

    /**
     * Number of divisions in this army
     */
    private List<ArmyDivision> divisions;

    /**
     * Start army size
     */
    private int startSize;

    /**
     * Flag that indicates that an army has surrendered
     */
    private boolean surrendered;

    /**
     *  Flag that indicates that army has been destroyed
     */
    private boolean destroyed;

    /**
     * Creates army of specified size, with given number of
     * army divisions. If army size is less than number of
     * army divisions then that number will be scale down to
     * army size and each division will get one soldier.
     *
     * Army size is allowed to be in range of <code>MIN_ARMY_SIZE</code>
     * to <code>MAX_ARMY_SIZE</code>.
     *
     * Number of army divisions is allowed to be in range of
     * <code>MIN_DIVISION_SIZE</code> to <code>MAX_DIVISION_SIZE</code>
     *
     *
     * @param armySize size of the army
     * @param numOfDivisions number of army divisions
     */
    public Army(int armySize, int numOfDivisions) {
        if (armySize < MIN_ARMY_SIZE || armySize > MAX_ARMY_SIZE) {
            throw new BattleException("Invalid army size");
        }

        if (numOfDivisions < MIN_DIVISION_SIZE || numOfDivisions > MAX_DIVISION_SIZE) {
            throw new BattleException("Invalid field size");
        }

        if (armySize < numOfDivisions) { //Uniformly distribute soldiers across all divisions
            numOfDivisions = armySize;
        }

        surrendered = false;
        destroyed  = false;

        startSize = numOfDivisions;

        divisions  = new ArrayList<>(numOfDivisions);

        //Average number of soldiers per division
        int avg = Math.floorDiv(armySize, numOfDivisions);

        for (int i = 0; i < numOfDivisions; i++) {
            ArmyDivision division = new ArmyDivision(this);
            division.addSoldiers(SoldierFactory.createSoldiers(avg));
            divisions.add(division);
        }

        int notAdded = armySize - avg * numOfDivisions;  //If army is not of size army size fill it
        if(notAdded != 0) {
            divisions.get(numOfDivisions - 1).addSoldiers(SoldierFactory.createSoldiers(notAdded));
        }

        for (ArmyDivision div : divisions) {
            div.addDivisionDestroyListener(() -> {
                divisions.remove(div);
                if(divisions.isEmpty()) {
                    destroyed = true;
                }
            });
        }
    }

    /**
     * Returns army divisions.
     * @return army divisions
     */
    public List<ArmyDivision> getDivisions() {
        return divisions;
    }


    /**
     * Returns a start size of army.
     * @return a start size of army.
     */
    public int getStartSize() {
        return startSize;
    }

    /**
     * Returns a number of alive soldiers in army.
     * @return a number of alive soldiers in army.
     */
    public int getSize() {
        int sum = 0;
        for(ArmyDivision d : divisions) {
            sum += d.getSoldiers().size();
        }
        return sum;
    }

    /**
     *  Sets surrender flag to true.
     */
    public void surrender() {
        surrendered = true;
    }

    /**
     * Returns true if this army has surrendered, false
     * otherwise.
     *
     * @return true if this army has surrendered, false
     *         otherwise.
     */
    public boolean hasSurrendered() {
        return surrendered;
    }

    /**
     * Returns true if this army has been destroyed, false
     * otherwise.
     *
     * @return true if this army has been destroyed, false
     * otherwise.
     */
    public boolean isDestroyed() {
        return destroyed;
    }
}
