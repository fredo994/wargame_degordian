package hr.fredi.battlegame.Util;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import static java.nio.file.StandardOpenOption.*;

/**
 * Singleton class used to log battle events.
 *
 * Created by Fredi Šarić on 09.12.15.
 */
public class BattleLogger {

    /**
     * Instance of BattleLogger
     */
    private static BattleLogger instance = new BattleLogger();

    /**
     * BattleLogs
     */
    private List<String> battleLogs;

    /**
     * Victory log
     */
    private String battleVictoryLog;

    /**
     * Prepared Log
     */
    private String preparedLog;

    /**
     * Constructs BattleLogger
     */
    private BattleLogger() {
        battleLogs = new ArrayList<>();
    }

    /**
     * Returns instance of BattleLogger
     * @return instance of BattleLogger
     */
    public static BattleLogger getInstance() {
        return instance;
    }

    /**
     * Stores battle log
     * @param log battle log
     */
    public void addLog(String log) {
        battleLogs.add((preparedLog == null ? "" : preparedLog) + log);
        preparedLog = "";
    }

    /**
     * Prepares log.
     * @param log log
     */
    public void prepareLog(String log) {
        if( preparedLog == null) {
            preparedLog = log;
            return;
        }
        preparedLog += log;
    }

    /**
     * Returns a list of battle logs
     * @return a list of battle logs
     */
    public List<String> getBattleLogs() {
        return battleLogs;
    }

    /**
     * Sets victory log.
     * @param victoryLog log showing who won the battle
     */
    public void setBattleVictoryLog(String victoryLog) {
        this.battleVictoryLog = victoryLog;
    }

    /**
     * Returns victory log.
     * @return victory log.
     */
    public String getBattleVictoryLog() {
        return battleVictoryLog;
    }

    /**
     * Saves log to disk in given file.
     * @param filePath path to the file to save battle log.
     * @throws IOException @See Files.write(...);
     */
    public void saveLog(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        Files.write(path, battleLogs, Charset.defaultCharset(),CREATE, TRUNCATE_EXISTING, WRITE);
    }
}
