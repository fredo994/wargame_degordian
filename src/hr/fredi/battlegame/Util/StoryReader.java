package hr.fredi.battlegame.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that reads a battle story and store it's content.
 *
 * Battle story must consist of story intro, story end and
 * story outcome. All parts must be separated by an empty line.
 *
 * There are 6 possible outcomes of the story
 * indicated in <code>Battle</code> class.
 * Story can have comments and they are recognized by firs
 * character in the line that has to be '%'.
 *
 * Story outcome is numerated from 1 to 6 and every outcome can
 * be multiple line long. Outcome must start after end of the story
 * and outcome number must be divide from outcome message by '#'
 * delimiter.
 *
 * Created by Fredi Šarić on 11.12.15.
 */
public class StoryReader {

    /**
     * Story intro
     */
    private List<String> intro;

    /**
     * End of story
     */
    private List<String> storyEnd;

    /**
     * Map of story outcomes
     */
    private Map<Integer, List<String>> response;

    /**
     * Creates a story reader.
     */
    public StoryReader() {
        intro = new ArrayList<>();
        storyEnd = new ArrayList<>();
        response = new HashMap<>();
    }

    /**
     * Fills a story reader with a content of given story.
     *
     * @param story story being read.
     */
    public void read(List<String> story) {
        int count = 0;
        for(String line : story) {
            if (line.startsWith("%")) continue; //Comment

            if(line.isEmpty()) {
                count++;
                continue;
            }

            if(count == 0) intro.add(line);
            if(count == 1) storyEnd.add(line);
            if(count == 2) {
                String[] parts = line.split("#");
                int res = Integer.parseInt(parts[0].trim());
                List<String> resLines = response.get(res);
                if(resLines == null) {
                    resLines = new ArrayList<>();
                    response.put(res, resLines);
                }
                resLines.add(parts[1]);
            }
        }
    }

    /**
     * Returns a story intro.
     * @return a story intro.
     */
    public List<String> getIntro() {
        return intro;
    }

    /**
     * Returns a story end
     * @return a story end
     */
    public List<String> getStoryEnd() {
        return storyEnd;
    }

    /**
     * Returns a story outcome mapped to given index
     * @param i index of a story end.
     * @return a story outcome mapped to given index
     */
    public List<String> getResponse(int i) {
        return response.get(i);
    }
}
