package hr.fredi.battlegame.Util;

import hr.fredi.battlegame.soldiers.Soldier;
import hr.fredi.battlegame.soldiers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Soldier factory.
 *
 * Created by Fredi Šarić on 10.12.15.
 */
public class SoldierFactory {

    /**
     * Random number generator.
     */
    private static Random rnd = new Random();

    /**
     * Type of soldiers and their factory methods.
     */
    /*
        This is implemented this way because all soldier
        parameters are controlled here and have the ability to
        iterate over all of the enums. Think of this enums as
        little factories.
     */
    private enum Soldiers {
        //Sum of all chances is 1

        TROOPER(0.3) {  //40% chance of being chosen
            Soldier getSoldier() {
                return new Trooper(15, 20, "Trooper");
            }
        },
        SNIPER(0.1) {  //10% chance of being chosen
            private double MIN = 0.5;
            private double MAX = 0.9;

            Soldier getSoldier() {
                return new Sniper(Integer.MAX_VALUE, 1, "Sniper",
                                  MIN + (MAX - MIN) * rnd.nextDouble(), rnd);
            }
        },
        OLD_MAN(0.1) {  //10% chance of being chosen
            Soldier getSoldier() {
                return new OldMan(5, 1, "Old man", rnd);
            }
        },
        KAMIKAZE(0.15) {  //5% chance of being chosen
            Soldier getSoldier() { return new Kamikaze(0, 30, "Kamikaze", rnd); }
        },
        CIVIL(0.1) {  //10% chance of being chosen
            Soldier getSoldier() {
                return new Civil(5, 20, "Civilian");
            }
        },
        GENERAL(0.05) { //5% chance of being chosen
            private double MIN_PEACE = 0.0;
            private double MAX_PEACE = 0.15;

            private double MIN_SURR  = 0.0;
            private double MAX_SURR  = 0.1;

            private double MIN_SURR_CHANCE = 0.0;
            private double MAX_SURR_CHANCE = 0.5;

            Soldier getSoldier() {
                double peaceChance = MIN_PEACE + (MAX_PEACE - MIN_PEACE) * rnd.nextDouble();
                double surrConsid  = MIN_SURR  + (MAX_SURR  - MIN_SURR)  * rnd.nextDouble();
                double surrChance  = MIN_SURR_CHANCE + (MAX_SURR_CHANCE - MIN_SURR_CHANCE) * rnd.nextDouble();
                return new General(30, 100, "General", peaceChance, surrConsid, surrChance, rnd );
            }
        },
        HIPPIE(0.2) {  //20% chance of being chosen
            Soldier getSoldier() {
                return new Hippie(0, 50,"Hippie");
            }
        };

        /**
         * Chance that soldier will be chosen.
         */
        private final double chance;

        /**
         * Constructs Soldier enum
         * @param chance chance that this type of soldier will be chosen
         */
        Soldiers(double chance) {
            this.chance = chance;
        }

        /**
         * Factory method that create a soldier.
         * @return new soldier.
         */
        abstract Soldier getSoldier();
    }

    /**
     * Creates a soldier. Soldier are chosen proportionally based
     * on their chance.
     * @return new soldier.
     */
    public static Soldier createSoldier() {
        double rndVal = rnd.nextDouble();
        double currSum = 0;

        for(Soldiers s : Soldiers.values()) {
            currSum += s.chance;
            if(currSum >= rndVal) {
                return s.getSoldier();
            }
        }

        return null; //http://sd.keepcalm-o-matic.co.uk/i/keep-calm-it-s-never-gonna-happen.png
    }

    /**
     * Creates list of <code>size</code> soldiers.
     * @param size number of soldiers
     * @return list of soldiers.
     */
    public static List<Soldier> createSoldiers(int size) {
        if( size < 1) throw new IllegalArgumentException("Size must be positive.");

        List<Soldier> soldiers = new ArrayList<>(size);
        for(int i = 0; i < size; i++) {
            soldiers.add(createSoldier());
        }

        return soldiers;
    }

}
