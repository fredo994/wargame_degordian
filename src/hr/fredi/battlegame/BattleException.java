package hr.fredi.battlegame;

/**
 *
 * Created by Fredi Šarić on 09.12.15.
 */
public class BattleException extends RuntimeException {

    public BattleException() {
        super();
    }

    public BattleException(String message) {
        super(message);
    }

    public BattleException(String message, Throwable cause) {
        super(message, cause);
    }

    public BattleException(Throwable cause) {
        super(cause);
    }
}
